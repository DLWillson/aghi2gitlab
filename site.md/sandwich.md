# Sandwich

Serving size = 1

## Ingredients

- 2 slices of bread of choice (I prefer sourdough)
- 2T softened, salted butter
- 4 slices of chosen cheese (I prefer cheddar and swiss)
- 2 slices of crisp bacon (I prefer smokey, jalapeno)
- 2 thin slices of ripe tomato
- 1/3 ripened avocado (thinly sliced)


1. Preheat cast iron skillet (or skilled or your choice) on medium until water dropped into the pan dances
2. Place butter on one slice of your bread.  Place on preheated skillet
3. Place 2 slices of cheese to fit your bread (you may need to tear to fit appropriately)
4. Wait for 30-60 seconds for cheese to start to melt  
5. Add tomatoes, bacon, avocado, and last 2 slices of cheese (again you may need to tear to fit appropriately)
6. Butter other slice of bread and place the UNBUTTERED side onto the sandwhich
7. Carefully use spatulat to slide under (now toasted) bread.  Hold the top with your dominate hand and flip the sandwhich to toast the other side
8. The bread should be golden!
9. Once the cheese has melted pull from the skilled.  Cut in chosen style (I prefer triangles)
10. Eat and enjoy! 

